import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http : HttpClient) {

  }

  getListUsers(){
    return this.http.get('http://localhost:8080/JavaRestApi/api/user')
  }

}
