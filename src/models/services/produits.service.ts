import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProduitsService {
  private _productUrl = './assets/api/products/products.json';
  constructor(private http : HttpClient) { }

  getProductList(){
    return this.http.get(this._productUrl);
  }
}
