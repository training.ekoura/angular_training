export interface IProduct {
    productId: number;
    productName: string;
    productCode: string;
    releaseDate: string;
    price: number;
    description: string;
    starRating: number;
    imageUrl: string;
}


export class MonProduit {
    
    private _libelleProduit: string;
    public get libelleProduit(): string {
        return this._libelleProduit;
    }
    public set libelleProduit(value: string) {
        this._libelleProduit = value;
    }

    private _dispoDay: string;
    public get dispoDay(): string {
        return this._dispoDay;
    }
    public set dispoDay(value: string) {
        this._dispoDay = value;
    }
    
    private _price: number;
    public get price(): number {
        return this._price;
    }
    public set price(value: number) {
        this._price = value;
    }

    private _survey: number;
    public get survey(): number {
        return this._survey;
    }
    public set survey(value: number) {
        this._survey = value;
    }
    
    private _imageUrl: string;
    public get imageUrl(): string {
        return this._imageUrl;
    }
    public set imageUrl(value: string) {
        this._imageUrl = value;
    }

    constructor( 
        libelleProduit : string,
        dispoDay : string,
        price : number,
        survey : number,
        imageUrl : string){
            this._libelleProduit = libelleProduit;
            this._dispoDay = dispoDay;
            this._price = price;
            this._survey = survey;
            this._imageUrl = imageUrl;
        }
}


export interface IUser {
    userId : number;
    userName : string;
    userLastName : string;
    userAge : number;
    userCity : string;
}

export class User  {
    private _userId: number;    
    public get userId(): number {
        return this._userId;
    }
    public set userId(value: number) {
        this._userId = value;
    }

    private _userName: string;
    public get userName(): string {
        return this._userName;
    }
    public set userName(value: string) {
        this._userName = value;
    }

    private _userLastName: string;
    public get userLastName(): string {
        return this._userLastName;
    }
    public set userLastName(value: string) {
        this._userLastName = value;
    }

    private _userAge: number;
    public get userAge(): number {
        return this._userAge;
    }
    public set userAge(value: number) {
        this._userAge = value;
    }

    private _userCity: string;
    public get userCity(): string {
        return this._userCity;
    }
    public set userCity(value: string) {
        this._userCity = value;
    }
}


export class Product implements  IProduct{
    private _productId: number;    
    public get productId(): number {
        return this._productId;
    }
    public set productId(value: number) {
        this._productId = value;
    }
    private _productName: string;
    public get productName(): string {
        return this._productName;
    }
    public set productName(value: string) {
        this._productName = value;
    }

    private _productCode: string;
    public get productCode(): string {
        return this._productCode;
    }
    public set productCode(value: string) {
        this._productCode = value;
    }
    private _releaseDate: string;
    public get releaseDate(): string {
        return this._releaseDate;
    }
    public set releaseDate(value: string) {
        this._releaseDate = value;
    }
    private _price: number;
    public get price(): number {
        return this._price;
    }
    public set price(value: number) {
        this._price = value;
    }
    private _description: string;
    public get description(): string {
        return this._description;
    }
    public set description(value: string) {
        this._description = value;
    }
    private _starRating: number;
    public get starRating(): number {
        return this._starRating;
    }
    public set starRating(value: number) {
        this._starRating = value;
    }
    private _imageUrl: string;
    public get imageUrl(): string {
        return this._imageUrl;
    }
    public set imageUrl(value: string) {
        this._imageUrl = value;
    }

    constructor(
                        productId: number, 
                        productName: string,
                        productCode: string,
                        releaseDate: string,
                        price: number,
                        description: string,
                        starRating: number,
                        imageUrl: string
                    ){
        this._productId = productId;
        this._productName = productName;
        this.productCode = productCode;
        this._releaseDate = releaseDate;
        this._price = price;
        this._description = description;
        this._starRating = starRating;
        this._imageUrl = imageUrl;
    }
    
}