import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { FirstcompoComponent } from './firstcompo/firstcompo.component';
import { HomeComponent } from './site/home/home.component';
import { TemplateProductComponent } from './shared/template-product/template-product.component';
import { CatalogueComponent } from './site/catalogue/catalogue.component';
import { ContactComponent } from './site/contact/contact.component';
import { ConnexionComponent } from './site/connexion/connexion.component';
import { InscriptionComponent } from './site/inscription/inscription.component';
import { ProductService } from './../models/services/product.service';
import { ProduitsService } from './../models/services/produits.service';
import { UserService } from './../models/services/user.service';
import { HttpClientModule } from '@angular/common/http';
import { RatingComponent } from './shared/rating/rating.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstcompoComponent,
    HomeComponent,
    TemplateProductComponent,
    CatalogueComponent,
    ContactComponent,
    ConnexionComponent,
    InscriptionComponent,
    RatingComponent
  ],

  imports: [
    BrowserModule, 
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
        { path: 'home', component: HomeComponent },
        { path: 'catalogue', component: CatalogueComponent },
        { path: 'contact', component: ContactComponent },
        { path: 'connexion', component: ConnexionComponent },
        { path: 'inscription', component: InscriptionComponent },
        { path: '', redirectTo: 'home', pathMatch: 'full'},
        { path: '**', redirectTo: 'home', pathMatch: 'full'}
    ])
  ],

  providers: [ProductService, UserService, ProduitsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
