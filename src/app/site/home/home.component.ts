import { Component, OnInit } from '@angular/core';
import { Product, IProduct } from 'src/models/product';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // List des produits
  products: IProduct[] = [];

  constructor() { 
    const p1 = new Product(10, "Sandwitch au jambon", "2AAED", "12/04/2018", 3.5, "A commander aujourd'hui", 3, "https://previews.123rf.com/images/lsantilli/lsantilli1204/lsantilli120400224/13417289-grand-sandwich-au-jambon-fromage-tomates-et-salade-sur-du-pain-grill%C3%A9.jpg");
    const p2 = new Product(11, "Hamburgeur", "21AED", "12/04/2018", 4.5, "A commander aujourd'hui", 4, "https://static.cuisineaz.com/400x320/i91705-hamburger.jpg");
    const p3 = new Product(12, "Sandwitch", "22AED", "12/04/2018", 3.99, "A commander aujourd'hui", 5, "https://prods3.imgix.net/images/articles/2017_04/Feature-restaurant-butcher-bakery-shops2.jpg?auto=format%2Ccompress&ixjsv=2.2.3&w=670");
    const p4 = new Product(13, "Tomate", "23AED", "12/04/2018", 23, "A commander aujourd'hui", 23, "https://img-31.ccm2.net/ePhOXpy7Mn2osajKIZ_vc1nRYWc=/1240x/smart/7386a53071e34543816a41022375321f/ccmcms-hugo/10558281.jpg");
    const p5 = new Product(14, "Tomate", "23AED", "12/04/2018", 23, "A commander aujourd'hui", 23, "https://img-31.ccm2.net/ePhOXpy7Mn2osajKIZ_vc1nRYWc=/1240x/smart/7386a53071e34543816a41022375321f/ccmcms-hugo/10558281.jpg");
      
    this.products.push(p1);
    this.products.push(p2);
    this.products.push(p3);

  }

  ngOnInit() {
  }

}
