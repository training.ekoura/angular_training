import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { MonProduit, IProduct, Product, User } from 'src/models/product';
import { ProductService } from './../../../models/services/product.service';
import { UserService } from './../../../models/services/user.service';
import { ProduitsService } from './../../../models/services/produits.service';
import { Produit } from 'src/models/produit';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})

export class CatalogueComponent implements OnInit, OnDestroy, OnChanges {
  ratingClicked: number;
  itemIdRatingClicked: string;
  listMonProduit: MonProduit[]=[];

  filteredProducts  : IProduct[]=[];
  products          : IProduct[]=[];


  showImage: boolean = false;
  imageWidth: number = 50;
  imageMargin: number = 2;
  users : User[] = [];
  ListDesProduits : Produit[] = [];
  
  constructor(
                private serviceProduit : ProductService, 
                private userService : UserService,
                private produitsService : ProduitsService
              ) { 

    /*const mp1 = new MonProduit("Croissant ", "L - Ma - V", 12.5, 5, "https://img-31.ccm2.net/ePhOXpy7Mn2osajKIZ_vc1nRYWc=/1240x/smart/7386a53071e34543816a41022375321f/ccmcms-hugo/10558281.jpg");
    const mp2 = new MonProduit("Croissant 1", "L - Me - V", 12.5, 5, "https://img-31.ccm2.net/ePhOXpy7Mn2osajKIZ_vc1nRYWc=/1240x/smart/7386a53071e34543816a41022375321f/ccmcms-hugo/10558281.jpg");
    const mp3 = new MonProduit("Croissant 2", "L - J - V", 12.5, 5, "https://img-31.ccm2.net/ePhOXpy7Mn2osajKIZ_vc1nRYWc=/1240x/smart/7386a53071e34543816a41022375321f/ccmcms-hugo/10558281.jpg");
    const mp4 = new MonProduit("Croissant 3", "L - Ma - V", 12.5, 5, "https://img-31.ccm2.net/ePhOXpy7Mn2osajKIZ_vc1nRYWc=/1240x/smart/7386a53071e34543816a41022375321f/ccmcms-hugo/10558281.jpg");
    const mp5 = new MonProduit("Croissant 4", "Ma - V", 12.5, 5, "https://img-31.ccm2.net/ePhOXpy7Mn2osajKIZ_vc1nRYWc=/1240x/smart/7386a53071e34543816a41022375321f/ccmcms-hugo/10558281.jpg");
    const mp6 = new MonProduit("Croissant 5", "L - V", 12.5, 5, "https://img-31.ccm2.net/ePhOXpy7Mn2osajKIZ_vc1nRYWc=/1240x/smart/7386a53071e34543816a41022375321f/ccmcms-hugo/10558281.jpg");
    const mp7 = new MonProduit("Croissant 6", "L - Me - V", 12.5, 5, "https://img-31.ccm2.net/ePhOXpy7Mn2osajKIZ_vc1nRYWc=/1240x/smart/7386a53071e34543816a41022375321f/ccmcms-hugo/10558281.jpg");
    this.listMonProduit.push(mp1);
    this.listMonProduit.push(mp2);
    this.listMonProduit.push(mp3);
    this.listMonProduit.push(mp4);
    this.listMonProduit.push(mp5);
    this.listMonProduit.push(mp6);
    this.listMonProduit.push(mp7);

    console.log(this.listMonProduit);*/

  }

  _rechercher: string;
  get rechercher(): string {
    return this._rechercher;
  }
  set rechercher(value: string) {
    this._rechercher = value;
    this.filteredProducts = this.rechercher ? this.performFilter(this.rechercher) : this.products;
  }
  
  _listFilter: string;
  get listFilter(): string {
      return this._listFilter;
  }
  set listFilter(value: string) {
      this._listFilter = value;
      this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.products;
  }

  ngOnInit() {
    this.serviceProduit.getProducts().subscribe((data) => {
      this.products = data;
      this.filteredProducts = data;
    });

    // Appel list des users.
    this.userService.getListUsers().subscribe((data) =>{
      this.users = data as User[];
    })

    this.produitsService.getProductList().subscribe((data)=> {
      console.log("Appel du services des produits : ==>", data);

      this.ListDesProduits = data as Produit[];
    })

    //this.products = this._ProductService.getProducts();
    //this.filteredProducts = this.products;
  }

  ngOnDestroy(): void {
    console.log("ngOnDestroy ===>  ");
  }

  ngOnChanges():void {
    console.log("ngOnChanges ===>  ");
  }

  performFilter(filterBy: string): IProduct[] {
    return this.products.filter((product:IProduct) =>
                                product.productName.toLocaleLowerCase().indexOf(filterBy.toLocaleLowerCase()) !== -1);
  }

  toggleImage(): void {
    this.showImage = !this.showImage;
  }

  ratingComponentClick(clickObj: any): void {
    /*const item = this.items.find(((i: any) => i.id === clickObj.itemId));
    if (!!item) {
      item.rating = clickObj.rating;
      this.ratingClicked = clickObj.rating;
      this.itemIdRatingClicked = item.company;
    }*$*/

  }
}
