import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-firstcompo',
  templateUrl: './firstcompo.component.html',
  styleUrls: ['./firstcompo.component.css']
})

export class FirstcompoComponent implements OnInit {
  

  // Les inputs 
  @Input() prenom : string ; 
  @Input() nom : string;

  // Event pour envoyer de l'info au parent.
  @Output() ratingNom: EventEmitter<string>     = new EventEmitter<string>();
  @Output() ratingPrenom: EventEmitter<string>  = new EventEmitter<string>();


  numeroDeVersion : number = 12;
  title : string = "First app";

  constructor() {
    console.log("Affichage de la valeur dans le constructeur ==>",this.numeroDeVersion);
    this.numeroDeVersion = 34;
    console.log("Affichage de la valeur dans le constructeur ==>",this.numeroDeVersion);
    this.title = "Second app";
  }

  ngOnInit() {
    this.numeroDeVersion = 45;
    console.log("Affichage de la valeur dans le ngOnInit ==>",this.numeroDeVersion);
  }

  NotifyNom(){
    console.log(this.nom);
    this.ratingNom.emit(this.nom);
  }

  NotifyPrenom(){
    console.log(this.prenom);
    this.ratingPrenom.emit(this.prenom);
  }

}
