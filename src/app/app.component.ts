import { Component } from '@angular/core';
import { p } from '@angular/core/src/render3';
import { IProduct, User, Product, MonProduit } from 'src/models/product';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  // Mes propriétés
  title = '';
  lastName = '';
  ageName = 0;
  cityName = '';
  divfirstcompo : string = "divfirstcompo";
  firstUser : User = new User();
  arrayAge : number[] = [];
  heroes = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado'];
  isAuth:boolean = true;
  imageSource : string = "https://img-31.ccm2.net/ePhOXpy7Mn2osajKIZ_vc1nRYWc=/1240x/smart/7386a53071e34543816a41022375321f/ccmcms-hugo/10558281.jpg";
  tailleImage : number = 300;
  showErrorMessage : boolean = false;
  showErrorMessageUserName : boolean = false;
  showErrorMessageLastName : boolean = false;
  showErrorMessageAgeName : boolean = false;
  showErrorMessageCityName : boolean = false;
  imageWidth: number = 50;
  imageMargin: number = 2;
  showImage: boolean = false;
  errorMessage: string;
  listMonProduit : MonProduit[] = [];
  nomParent : string = "Nom parent ";
  prenomParent : string = "Prénom parent";


  constructor() {

    setTimeout(
      ()=>{

        //this.isAuth = false;
        //this.imageSource = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Jack-o%27-Lantern_2003-10-31.jpg/250px-Jack-o%27-Lantern_2003-10-31.jpg";
        //this.tailleImage = 600;

      }, 4000
    )

  }

  enregistrer()  {

    this.showErrorMessage = !this.showErrorMessage;
  
    // Gestion des messages d'erreurs du champ userName
    if(this.title.length == 0)
      this.showErrorMessageUserName = true;
    else
      this.showErrorMessageUserName = false;

    // Gestion des messages d'erreurs pour le lastName
    if(this.lastName.length == 0)
    this.showErrorMessageLastName = true;
    else
    this.showErrorMessageLastName = false;
    
    // Gestion des messages d'erreurs pour le Age
    if(this.ageName > 0)
      this.showErrorMessageAgeName = true;
    else
      this.showErrorMessageAgeName = false;
    
    // Gestion des messages d'erreurs pour le cityName
    if(this.cityName.length == 0)
      this.showErrorMessageCityName = true;
    else
      this.showErrorMessageCityName = false;
    //papp
  }

  toggleImage(): void {
    this.showImage = !this.showImage;
  }

  onRatingNom(newNom: string){
    console.log("Parent eventNomData ==> ", newNom);
    this.nomParent = newNom;
  }
  
  
  onRatingPrenom(newPrenom: string){
    console.log("Parent eventPreNomData ==> ", newPrenom);
    this.prenomParent = newPrenom;
  }
}
