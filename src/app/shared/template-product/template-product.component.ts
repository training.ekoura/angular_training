import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/models/product';

@Component({
  selector: 'app-template-product',
  templateUrl: './template-product.component.html',
  styleUrls: ['./template-product.component.css']
})
export class TemplateProductComponent implements OnInit {

  @Input() produit:Product;
  
  constructor() { 
  
  }
  
  ngOnInit() {
  }


}
